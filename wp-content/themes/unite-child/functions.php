<?php
function unite_child_enqueue_styles() {
	wp_enqueue_style('unite-style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('unite-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		['unite-bootstrap', 'unite-icons', 'unite-style'],
		wp_get_theme()->get('Version')
	);
}
add_action('wp_enqueue_scripts', 'unite_child_enqueue_styles');

/**
 * shortcodes
 */
function unite_recent_films_code($atts) {
	$a = shortcode_atts([
		'number' => 5,
	], $atts, 'recent_films');

	$query = new WP_Query([
		'post_type' => 'film',
		'posts_per_page' => $a['number'],
	]);

	if ($query->have_posts()) {
		$output = "<ul>";
		while ($query->have_posts()) {
			$query->the_post();
			$output .= "<li><a href=\"" . get_the_permalink() . "\">" . get_the_title() . "</li>";
		}
		$output .= "</ul>";
		wp_reset_postdata();
	} else {
			$output = "No films found.";
	}
	return $output;
}
add_shortcode('recent_films', 'unite_recent_films_code');
